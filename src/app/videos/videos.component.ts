import {Component, Input, OnInit} from '@angular/core';
import {LoggerLevel, LoggerService} from '../logger.service';
import {VideoService} from '../video-service';
import {Common} from '../common';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.css']
})
export class VideosComponent implements OnInit {
  @Input('profileKey') profileKey: string;

  // all action are done assuming that the user is connected (authenticated)

  constructor(
    private logger: LoggerService,
    private videoService: VideoService
  ) { }

  listVideos() {
    try {
      this.videoService.listVideos(this.profileKey)
        .subscribe((res: any) => {
          if (Common.checkResponse(res)) {
            this.logger.log(res, LoggerLevel.DEBUG);

            this.videoService.videoList = res.content.list;

            this.logger.log(res.message, LoggerLevel.INFO);
          } else {
            this.logger.log(res.message, LoggerLevel.WARNING);
          }
        });
    } catch (e) {
      this.logger.error(e);
    }
  }

  // called when user's connected (see profile.component.html)
  ngOnInit() {
    if (this.profileKey == null) {
      this.logger.log('error while setting profile\'s key', LoggerLevel.ERROR);
    }

    // set the list of the videos when the user connect
    this.listVideos();
  }


}
