import {Component, Input, OnInit} from '@angular/core';
import {Video} from '../video';
import {LoggerLevel, LoggerService} from '../logger.service';
import {VideoService} from '../video-service';
import {config} from '../../assets/config';
import {Common} from '../common';

@Component({
  selector: 'app-video-unit',
  templateUrl: './video-unit.component.html',
  styleUrls: ['./video-unit.component.css']
})
export class VideoUnitComponent implements OnInit {
  @Input('video')video: Video;
  @Input('profileKey')profileKey: string;

  constructor(
    private logger: LoggerService,
    private videoService: VideoService
  ) { }

  ngOnInit() {
  }

  copyUrl(urlInput) {
    urlInput.select();
    document.execCommand('copy');
  }

  redirect() {
    window.open(this.video.url, '_blank');
  }

  removeVideo() {
    this.videoService.removeVideoFromList(this.profileKey, this.video)
      .subscribe((res: any) => {
        this.logger.log(res, LoggerLevel.DEBUG);
        if (Common.checkResponse(res)) {

          // remove from local list
          this.videoService.remove(this.video);

          this.logger.log(res.message, LoggerLevel.INFO);
        } else {
          this.logger.log(res.message, LoggerLevel.WARNING);
        }
      });
  }

  slideUpdateForm(panel) {
    const panelList = document.getElementsByClassName('panel');

    let i;
    for (i = 0; i < panelList.length; i++) {
      if (panelList[i] != panel) {
        // @ts-ignore
        panelList[i].style.display = 'none';
      }
    }

    if (panel.style.display === 'block') {
      panel.style.display = 'none';
    } else {
      panel.style.display = 'block';
    }
  }

  updateVideo(title, author, album, date) {
    const v = new Video();
    v.video_id = this.video.video_id;
    v.url = this.video.url;
    v.title = title.value;
    v.author = author.value;
    v.album = album.value;
    v.date = date.value;

    this.videoService.updateVideo(this.profileKey, v)
      .subscribe((res: any) => {
        this.logger.log(res, LoggerLevel.DEBUG);
        if (Common.checkResponse(res)) {

          // updating infos of current component
          this.video = res.content.video;

          // updating the list
          this.videoService.update(this.video);

          this.logger.log(res.message, LoggerLevel.INFO);
          title.value = '';
          author.value = '';
          album.value = '';
          date.value = '';
        } else {
          this.logger.log(res.message, LoggerLevel.WARNING);
        }
      });
  }

}
