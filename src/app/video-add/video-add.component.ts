import {Component, Input, OnInit} from '@angular/core';
import {VideoService} from '../video-service';
import {LoggerLevel, LoggerService} from '../logger.service';
import {Common} from '../common';

@Component({
  selector: 'app-video-add',
  templateUrl: './video-add.component.html',
  styleUrls: ['./video-add.component.css']
})
export class VideoAddComponent implements OnInit {
  @Input('profileKey') profileKey: string;


  constructor(
    private logger: LoggerService,
    private videoService: VideoService
  ) { }

  ngOnInit() {
  }

  addVideo(videoIdInput) {
    try {
      const videoId = videoIdInput.value;
      if (!videoId || videoId == '') {
        this.logger.error('missing input');
      }
      this.videoService.addVideoToList(this.profileKey, videoId)
      .subscribe((res: any) => {
        if (Common.checkResponse(res)) {
          this.logger.log(res, LoggerLevel.DEBUG);
          const video = res.content.video;
          this.videoService.add(video);
          this.logger.log(res.message, LoggerLevel.INFO);
        } else {
          this.logger.log(res.message, LoggerLevel.WARNING);
        }
      });
    } catch (e) {
      this.logger.error(e);
    }
  }

}
