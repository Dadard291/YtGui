// model mapped from the api model (see api doc)
export class Video {
  video_id: string;
  url: string;
  title: string;
  author: string;
  album: string;
  date: Date;
}

export class VideoPayload {
  title: string;
  author: string;
  album: string;
  date: Date;
}
