import {Component, OnInit} from '@angular/core';
import {Profile} from '../profile';
import {ProfileService} from '../profile.service';
import {LoggerLevel, LoggerService} from '../logger.service';
import {config} from '../../assets/config';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  profile: Profile;
  isConnected: boolean;

  constructor(
    private profileService: ProfileService,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    this.profile = new Profile();
    this.isConnected = false;
  }

  copyInputMessage(userInput) {
    userInput.select();
    document.execCommand('copy');
  }

  signIn() {
    try {
      if (!this.profile.username || !this.profile.password) {
        this.logger.error('missing input');
        return;
      }

      this.profileService.signIn(this.profile.username, this.profile.password)
        .subscribe((keyRes: any) => {
          if (keyRes.status === config.api_response.OK) {
            this.logger.log(keyRes, LoggerLevel.DEBUG);

            this.profile.key = keyRes.content.key;
            this.connectAndLoadVideos();

            this.logger.log(keyRes.message, LoggerLevel.INFO);
          } else {
            this.logger.log(keyRes.message, LoggerLevel.WARNING);
          }
        });
    } catch (e) {
      this.logger.log(e, LoggerLevel.ERROR);
    }
  }

  signUp() {
    try {
      if (!this.profile.username || !this.profile.password) {
        this.logger.error('missing input');
        return;
      }

      this.profileService.signUp(this.profile.username, this.profile.password)
        .subscribe((keyRes: any) => {
          if (keyRes.status === config.api_response.OK) {
            this.logger.log(keyRes, LoggerLevel.DEBUG);

            this.profile.key = keyRes.content.key;
            this.connectAndLoadVideos();

            this.logger.log(keyRes.message, LoggerLevel.INFO);
          } else {
            this.logger.log(keyRes.message, LoggerLevel.WARNING);
          }
        });
    } catch (e) {
      this.logger.log(e, LoggerLevel.ERROR);
    }
  }

  deleteAccount() {
    try {
      if (this.isConnected === true) {
        this.profileService.deleteAccount(this.profile.username, this.profile.password)
          .subscribe((keyRes: any) => {
            if (keyRes.status === config.api_response.OK) {
              this.logger.log(keyRes, LoggerLevel.DEBUG);

              this.profile.key = keyRes.content.key;
              this.isConnected = false;

              this.logger.log(keyRes.message, LoggerLevel.INFO);
              this.profile.key = null;
            } else {
              this.logger.log(keyRes.message, LoggerLevel.WARNING);
            }
          });
      } else {
        this.logger.log(`not connected, sign in first`, LoggerLevel.ERROR);
      }
    } catch (e) {
      this.logger.error(e);
    }
  }

  updatePassword(input) {
    try {
      if (this.isConnected === true) {
        const new_password = input.value;
        if (!new_password || new_password == '') {
          this.logger.error('missing input');
          return;
        }
        this.profileService.updatePassword(this.profile.username, this.profile.password, new_password)
          .subscribe((keyRes: any) => {
            if (keyRes.status === config.api_response.OK) {
              this.logger.log(keyRes, LoggerLevel.DEBUG);
              this.logger.log(keyRes.message, LoggerLevel.INFO);
            } else {
              this.logger.log(keyRes.message, LoggerLevel.WARNING);
            }
          });
      } else {
        this.logger.error('not connected');
      }
    } catch (e) {
      this.logger.error(e);
    }
  }

  logOut() {
    if (this.isConnected === true) {

      this.profile.key = null;
      this.isConnected = false;

      this.logger.log(`${this.profile.username} logged out`, LoggerLevel.INFO);
    } else {
      this.logger.error(`not connected`);
    }
  }

  connectAndLoadVideos() {
    this.isConnected = true;
  }

  reveal() {
    const p = (document.getElementById('password') as HTMLInputElement);
    p.type = 'text';
    p.style.borderColor = 'red';
  }

  hide() {
    const p = (document.getElementById('password') as HTMLInputElement);
    p.type = 'password';
    p.style.borderColor = 'grey';
  }
}
