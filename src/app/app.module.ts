import { BrowserModule } from '@angular/platform-browser';

import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { VideosComponent } from './videos/videos.component';
import { ProfileComponent } from './profile/profile.component';
import {FormsModule} from '@angular/forms';
import { LoggerComponent } from './logger/logger.component';
import { VideoUnitComponent } from './video-unit/video-unit.component';
import { VideoAddComponent } from './video-add/video-add.component';

@NgModule({
  declarations: [
    AppComponent,
    VideosComponent,
    ProfileComponent,
    LoggerComponent,
    VideoUnitComponent,
    VideoAddComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
