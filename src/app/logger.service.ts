import { Injectable } from '@angular/core';
import {environment} from '../environments/environment';

export enum LoggerLevel {
  DEBUG,
  INFO,
  WARNING,
  ERROR
}

export class Msg {
  content: string;
  color: string;
  date: string;
}

@Injectable({
  providedIn: 'root'
})
export class LoggerService {
  messages: Msg[] = [];

  public log(msg: string, level: LoggerLevel) {

    const d = new Date().toLocaleString();

    if (environment.production == false) {
      console.log(msg);
    }

    let color;
    if (level == LoggerLevel.DEBUG) {
      return;
    } else if (level === LoggerLevel.INFO) {
      color = 'green';
    } else if (level === LoggerLevel.WARNING) {
      color = 'yellow';
    } else if (level === LoggerLevel.ERROR) {
      color = 'red';
    }

    const newMsg = new Msg();
    newMsg.content = msg;
    newMsg.color = color;
    newMsg.date = d;

    this.messages.push(newMsg);
  }

  public debug(msg: string) {
    if (environment.production == false) {
      console.log(msg);
    }
  }

  public error(msg: string) {
    const d = new Date().toLocaleString();

    if (environment.production == false) {
      console.log(msg);
    }

    const newMsg = new Msg();
    newMsg.content = msg;
    newMsg.color = 'red';
    newMsg.date = d;

    this.messages.push(newMsg);
  }

  clear() {
    this.messages = [];
  }
}
