import {Observable, of} from 'rxjs';
import {LoggerLevel, LoggerService} from './logger.service';

export class Common {
  public static checkResponse(responseObject: any): boolean {
    return responseObject.status === 'ok';
  }

  public static handleError<T>(logger: LoggerService, operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      logger.log(`${operation} : ${error.message}`, LoggerLevel.ERROR);
      return of(result as T);
    };
  }
}
