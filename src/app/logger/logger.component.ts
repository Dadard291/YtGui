import {AfterViewChecked, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {LoggerLevel, LoggerService} from '../logger.service';

@Component({
  selector: 'app-logger',
  templateUrl: './logger.component.html',
  styleUrls: ['./logger.component.css']
})
export class LoggerComponent implements OnInit, AfterViewChecked {

  constructor(public loggerService: LoggerService) { }

  ngOnInit() {
  }

  ngAfterViewChecked(): void {
    const l = document.getElementById('loggerContent');
    l.scrollTop = l.scrollHeight;
  }

  clear() {
    this.loggerService.clear();
  }

}
