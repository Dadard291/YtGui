import {Injectable} from '@angular/core';
import {config} from '../assets/config';
import {Observable, of} from 'rxjs';
import {LoggerLevel, LoggerService} from './logger.service';
import {HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {Common} from './common';


@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  private profileUrl = config.url.baseUrl + config.endpoints.apiEndpoint + config.endpoints.profileEndpoint;

  constructor(
    private logger: LoggerService,
    private http: HttpClient,
  ) { }

  signIn<Data>(username: string, password: string): Observable<string> {
    const getKeyUrl = `${this.profileUrl}?login_password=${username}:${password}`;
    return this.http.get<string>(getKeyUrl)
      .pipe(
        catchError(Common.handleError<any>(this.logger, 'signing in'))
      );
  }

  signUp<Data>(username: string, password: string): Observable<string> {
    const createAccUrl = `${this.profileUrl}?login_password=${username}:${password}`;
    return this.http.post<string>(createAccUrl, "")
      .pipe(
        catchError(Common.handleError<any>(this.logger, 'signing up'))
      );
  }

  deleteAccount<Data>(username: string, password: string): Observable<string> {
    const deleteAccUrl = `${this.profileUrl}?login_password=${username}:${password}`;
    return this.http.delete<string>(deleteAccUrl)
      .pipe(
        catchError(Common.handleError<any>(this.logger, 'deleting account'))
      );
  }

  updatePassword<Data>(username: string, password: string, new_password: string): Observable<string> {
    const updateAccUrl = `${this.profileUrl}?login_password=${username}:${password}&new_password=${new_password}`;
    return this.http.put<string>(updateAccUrl, '')
      .pipe(
        catchError(Common.handleError<any>(this.logger, 'updating password'))
      );
  }
}
