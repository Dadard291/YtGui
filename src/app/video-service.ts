import {Injectable} from '@angular/core';
import {LoggerLevel, LoggerService} from './logger.service';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {config} from '../assets/config';
import {catchError} from 'rxjs/operators';
import {Video, VideoPayload} from './video';
import {Common} from './common';

@Injectable({
  providedIn: 'root'
})
export class VideoService {
  videoList: Video[];

  private videoUrl = config.url.baseUrl + config.endpoints.apiEndpoint + config.endpoints.videoEndpoint;

  constructor(
    private logger: LoggerService,
    private http: HttpClient
  ) { }

  // operations involving api calls
  listVideos<Data>(profileKey: string) {
    const listVideoUrl = `${this.videoUrl}list/?key=${profileKey}`;
    return this.http.get<string>(listVideoUrl)
      .pipe(
        catchError(Common.handleError<any>(this.logger, 'listing video'))
      );
  }

  removeVideoFromList(profileKey: string, video: Video) {
    const deleteVideoUrl = `${this.videoUrl}${video.video_id}/?key=${profileKey}`;
    return this.http.delete<string>(deleteVideoUrl)
      .pipe(
        catchError(Common.handleError<any>(this.logger, 'deleting a video'))
      );
  }

  addVideoToList(profileKey: string, videoId: string) {
    const addVideoUrl = `${this.videoUrl}${videoId}/?key=${profileKey}`;
    return this.http.post<string>(addVideoUrl, '')
      .pipe(
        catchError(Common.handleError<any>(this.logger, 'adding video'))
      );
  }

  updateVideo(profileKey: string, video: Video) {
    const updateVideoUrl = `${this.videoUrl}${video.video_id}/?key=${profileKey}`;
    return this.http.put<string>(updateVideoUrl, JSON.stringify(video))
      .pipe(
        catchError(Common.handleError<any>(this.logger, 'updating video'))
      );
  }

  // operations for the local video list
  remove(video: Video) {
    const i = this.videoList.indexOf(video);
    this.logger.debug(`${i}`);
    if (i > -1) {
      this.videoList.splice(i, 1);
    }
  }

  add(video: Video) {
    this.videoList.push(video);
  }

  update(new_video: Video) {
    for (let i = 0; i < this.videoList.length; i++) {
      if (this.videoList[i].video_id === new_video.video_id) {
        this.videoList[i] = new_video;
      }
    }
  }
}
