import {environment} from '../environments/environment';

export let config = {
  endpoints: {
    apiEndpoint: 'yt_api/',
    profileEndpoint: 'profile/',
    videoEndpoint: 'video/'
  },

  url: {
    baseUrl: 'http://' + environment.env
  },

  api_response: {
    OK: 'ok',
    FAILED: 'ko'
  }

};
