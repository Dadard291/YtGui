#!/bin/sh

# Search main....bundle.js
# MAIN_JS=$(find "${HOME}" -type f -name main.*.js)

# Replace variables
# sed -i -e "s+{{API_BASE_URL}}+${API_BASE_URL}+g" "$MAIN_JS"
# sed -i -e "s+\"{{ENVIRONMENT_ORDER}}\"+${ENVIRONMENT_ORDER}+g" "$MAIN_JS"
# sed -i -e "s+\"{{ENVIRONMENT_MUST_MATCH}}\"+${ENVIRONMENT_MUST_MATCH}+g" "$MAIN_JS"

# Create log folder
mkdir  ${HOME}/logs

# Replace variables in nginx conf
sed -i -e "s+{HOME}+${HOME}+g" ${HOME}/nginx.conf
