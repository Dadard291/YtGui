FROM paas-dtr-dev.fr.world.socgen/dockerhub/nginx:1.15.10

COPY dist/yt-client-new /usr/share/nginx/html

EXPOSE 80
